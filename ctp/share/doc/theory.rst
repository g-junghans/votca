Theory and Background
#####################

.. _theory_morphology:

Morphology
**********

.. _theory_neighbor_list:

Neighbor list
*************

.. _theory_reorganization_energy:

Reorganization energy
*********************

.. _theory_site_energies:

Site energies
*************

.. _theory_electronic_couplings:

Electronic couplings
********************

.. _theory_rates:

Rates
*****

.. _theory_master_equation:

Master equation
***************

.. _theory_macroscopic_observables:

Macroscopic observables
***********************
