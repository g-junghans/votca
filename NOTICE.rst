Versatile Object-oriented Toolkit for Coarse-graining Applications
Copyright 2009-2022 The VOTCA Development Team

This product includes software developed at The VOTCA Development Team

This software in tools/ contains code, in particular the vec,
matrix, objectfactory and property class, which was
derived from a project by V. Ruehle.

This software in csg/ contains code, in particular function\_common.sh,
derived from a script collection by C. Junghans.

This software in ctp/ contains code, which was derived from VOTCA KMC and MOO project.

.. This software in xtp/ contains code, which was derived from VOTCA KMC,
.. MOO and CTP project.
