foreach(PROG csg_fluctuations csg_orientcorr csg_part_dist csg_partial_rdf csg_radii csg_sphericalorder csg_traj_force)
  file(GLOB ${PROG}_SOURCES ${PROG}*.cc)
  add_executable(${PROG} ${${PROG}_SOURCES})
  target_link_libraries(${PROG} votca_csg)
  install(TARGETS ${PROG} RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
  add_executable(VOTCA::${PROG} ALIAS ${PROG})
  set_property(TARGET votca_csg APPEND PROPERTY BINARIES "${PROG}")  
  #if (TXT2TAGS_FOUND AND BASH)
  #  add_custom_command(OUTPUT ${PROG}.man
  #    COMMAND ${CMAKE_CURRENT_BINARY_DIR}/${PROG} --help > ${PROG}.help
  #    COMMAND ${BASH} ${CMAKE_CURRENT_BINARY_DIR}/../../scripts/help2t2t ${PROG}.help > ${PROG}.t2t
  #    COMMAND ${TXT2TAGS_EXECUTABLE} -q -t man -i ${PROG}.t2t -o ${PROG}.man
  #    DEPENDS help2t2t_build ${PROG})
  #  add_custom_target(${PROG}_manpage DEPENDS ${PROG}.man)
  #  add_dependencies(manpages ${PROG}_manpage)
  #  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${PROG}.man DESTINATION ${CMAKE_INSTALL_MANDIR}/man1 RENAME ${PROG}.1)
  #  set_property(DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES ${PROG}.help ${PROG}.t2t)
  #endif(TXT2TAGS_FOUND AND BASH)
  if (BUILD_MANPAGES)
    add_custom_command(OUTPUT ${PROG}.man
      COMMAND $<TARGET_FILE:VOTCA::votca_help2doc> --name $<TARGET_FILE:VOTCA::${PROG}> --format groff --out ${PROG}.man
      COMMENT "Building ${PROG} manpage"
      DEPENDS $<TARGET_FILE:VOTCA::votca_help2doc> ${PROG})
    add_custom_target(${PROG}_manpage DEPENDS ${PROG}.man)
    add_dependencies(manpages ${PROG}_manpage)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${PROG}.man DESTINATION ${CMAKE_INSTALL_MANDIR}/man1 RENAME ${PROG}.1)
  endif()
  if(VOTCA_SPHINX_DIR)
    add_custom_command(OUTPUT ${VOTCA_SPHINX_DIR}/csg/${PROG}.rst
      COMMAND $<TARGET_FILE:VOTCA::votca_help2doc> --name $<TARGET_FILE:VOTCA::${PROG}> --format rst --out ${VOTCA_SPHINX_DIR}/csg/${PROG}.rst
      COMMENT "Building ${PROG} rst doc"
      DEPENDS $<TARGET_FILE:VOTCA::votca_help2doc> ${PROG})
    list(APPEND CSG_RST_FILES ${VOTCA_SPHINX_DIR}/csg/${PROG}.rst)
  endif(VOTCA_SPHINX_DIR)  
endforeach(PROG)

if(VOTCA_SPHINX_DIR)
  add_custom_target(doc-csg-rst-csgapps DEPENDS ${CSG_RST_FILES})
  add_dependencies(doc-csg doc-csg-rst-csgapps)
endif()
