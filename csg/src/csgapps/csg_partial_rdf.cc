/*
 * Copyright 2009 The VOTCA Development Team (http://www.votca.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef _RDFCALCULATOR_H
#define _RDFCALCULATOR_H

#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/symmetric.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/lexical_cast.hpp>
#include <math.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <votca/csg/beadlist.h>
#include <votca/csg/imcio.h>
#include <votca/csg/nblistgrid.h>
#include <votca/csg/csgapplication.h>
#include <votca/tools/average.h>
#include <votca/tools/histogramnew.h>
#include <votca/tools/property.h>
#include <votca/tools/rangeparser.h>

namespace votca {
namespace csg {
using namespace votca::tools;

/**
 * \brief class to calculate distribution functions and cross correlations for
 * inverse monte carlo
 *
 * This class calculates distribution functions as well as cross-correlations
 * for specific groups of interactions based on a given trajectory.
 *
 */
class RDFCalculator {
 public:
  RDFCalculator();
  ~RDFCalculator();

  void Initialize(void);

  /// load cg definitions file
  void LoadOptions(const string &file);

  /// begin coarse graining a trajectory
  void BeginEvaluate(Topology *top, Topology *top_atom);

  /// end coarse graining a trajectory
  void EndEvaluate();

  void WriteEvery(int write_every) { _write_every = write_every; }
  void DoBlocks(bool do_blocks) { _do_blocks = do_blocks; }
  void DoVolumeCorrection(bool do_vol_corr) { _do_vol_corr = do_vol_corr; }
  void SetSubvolRadius(double r) { _subvol_rad = r; }
  double AnalyticVolumeCorrection(double t) {

    cout << "DBG " << t << " "
         << 1.0 / 24.0 * (16.0 * t * t - 12.0 * t * t * t + t * t * t * t * t)
         << endl;
    return 1.0 / 24.0 * (16.0 * t * t - 12.0 * t * t * t + t * t * t * t * t);
  }

 protected:
  Average<double> _avg_vol;

  typedef Eigen::MatrixXd group_matrix;
  typedef Eigen::Block<group_matrix> pair_matrix;

  /// struct to store collected information for interactions
  struct interaction_t {
    int _index;
    Property *_p;
    HistogramNew _average;
    double _min, _max, _step;
    double _norm;
    bool _is_bonded;
    Average<double> _avg_beadlist_1_count;
    Average<double> _avg_beadlist_2_count;
  };

  // a pair of interactions which are correlated
  struct pair_t {
    interaction_t *_i1;
    interaction_t *_i2;
    int _offset_i, _offset_j;
    pair_matrix _corr;
    pair_t(interaction_t *i1, interaction_t *i2, int offset_i, int offset_j,
           const pair_matrix &corr);
  };

  /// struct to store collected information for groups (e.g. crosscorrelations)
  struct group_t {
    list<interaction_t *> _interactions;
    group_matrix _corr;
    vector<pair_t> _pairs;
  };

  /// the options parsed from cg definition file
  Property _options;
  // we want to write out every so many frames
  int _write_every;
  // we want do do block averaging -> clear averagings every write out
  bool _do_blocks;

  // number of frames we processed
  int _nframes;
  int _nblock;
  double _subvol_rad;
  vec _boxc;  // center of box
  bool _do_vol_corr;

  /// list of bonded interactions
  list<Property *> _bonded;
  /// list of non-bonded interactions
  list<Property *> _nonbonded;

  /// map ineteractionm-name to interaction
  map<string, interaction_t *> _interactions;
  /// map group-name to group
  map<string, group_t *> _groups;

  /// create a new interaction entry based on given options
  interaction_t *AddInteraction(Property *p);

  /// get group by name, creates one if it doesn't exist
  group_t *getGroup(const string &name);

  void WriteDist(const string &suffix = "");

  void ClearAverages();

  class Worker : public CsgApplication::Worker {
   public:
    vector<HistogramNew> _current_hists;
    RDFCalculator *_rdfcalculator;
    double _cur_vol;
    double _cur_beadlist_1_count;  // need to normalize to avg density for
                                   // subvol
    double _cur_beadlist_2_count;

    /// evaluate current conformation
    void EvalConfiguration(Topology *top, Topology *top_atom);
    /// process non-bonded interactions for given frame
    void DoNonbonded(Topology *top);
    /// process bonded interactions for given frame
    void DoBonded(Topology *top);
  };
  /// update the correlations after interations were processed
  void DoCorrelations(RDFCalculator::Worker *worker);

  bool _processed_some_frames;

 public:
  CsgApplication::Worker *ForkWorker();
  void MergeWorker(CsgApplication::Worker *worker);
};

inline RDFCalculator::pair_t::pair_t(RDFCalculator::interaction_t *i1,
                                     RDFCalculator::interaction_t *i2,
                                     int offset_i, int offset_j,
                                     const pair_matrix &corr)
    : _i1(i1), _i2(i2), _offset_i(offset_i), _offset_j(offset_j), _corr(corr) {}

}  // namespace csg
}  // namespace votca

namespace votca {
namespace csg {

RDFCalculator::RDFCalculator()
    : _write_every(0),
      _do_blocks(false),
      _subvol_rad(0),
      _do_vol_corr(false),
      _processed_some_frames(false) {}

RDFCalculator::~RDFCalculator() {}

// begin the coarse graining process
// here the data structures are prepared to handle all the data
void RDFCalculator::Initialize() {
  // do some output
  cout << "begin to calculate distribution functions\n";
  cout << "# of bonded interactions: " << _bonded.size() << endl;
  cout << "# of non-bonded interactions: " << _nonbonded.size() << endl;

  if (_bonded.size() + _nonbonded.size() == 0)
    throw std::runtime_error(
        "No interactions defined in options xml-file - nothing to be done");

  // initialize non-bonded structures
  for (list<Property *>::iterator iter = _nonbonded.begin();
       iter != _nonbonded.end(); ++iter) {
    interaction_t *i = AddInteraction(*iter);
    i->_is_bonded = false;
  }
};

void RDFCalculator::BeginEvaluate(Topology *top, Topology *top_atom) {
  matrix box;
  box = top->getBox();
  vec a = box.getCol(0);
  vec b = box.getCol(1);
  vec c = box.getCol(2);
  _boxc = a / 2 + b / 2 + c / 2;

  cout << "Using center of box: " << _boxc << endl;
  // we didn't process any frames so far
  _nframes = 0;
  _nblock = 0;
  _processed_some_frames = false;

  // initialize non-bonded structures
  for (list<Property *>::iterator iter = _nonbonded.begin();
       iter != _nonbonded.end(); ++iter) {
    string name = (*iter)->get("name").value();

    interaction_t &i = *_interactions[name];

    // count total species for ideal densities

    BeadList allbeads1, allbeads2;
    allbeads1.Generate(*top, (*iter)->get("type1").value());
    allbeads2.Generate(*top, (*iter)->get("type2").value());

    if (allbeads1.size() == 0)
      throw std::runtime_error("Topology does not have beads of type \"" +
                               (*iter)->get("type1").value() +
                               "\"\n"
                               "This was specified in type1 of interaction \"" +
                               name + "\"");
    if (allbeads2.size() == 0)
      throw std::runtime_error("Topology does not have beads of type \"" +
                               (*iter)->get("type2").value() +
                               "\"\n"
                               "This was specified in type2 of interaction \"" +
                               name + "\"");
    // calculate normalization factor for rdf

    /*if ((*iter)->get("type1").value() == (*iter)->get("type2").value())
        i._norm = 1. / (4. * M_PI * i._step * beads1.size()*(beads2.size() - 1.)
    / 2.); else i._norm = 1. / (4. * M_PI * i._step * beads1.size() *
    beads2.size());*/
    /*if ((*iter)->get("type1").value() == (*iter)->get("type2").value())
        i._norm = 1. / ( (beads2.size() - 1.) / 2.);
    else
        i._norm = 1. / (  beads2.size());*/

    if (_do_vol_corr) {
      cout << "Volume correction on" << endl;
      i._norm = 1. / (4.0 * M_PI * i._step);
      /* OLD STUFF FROM ANALYTIC
      double r5= _subvol_rad*_subvol_rad*_subvol_rad*_subvol_rad*_subvol_rad;

      if ((*iter)->get("type1").value() == (*iter)->get("type2").value())
          i._norm = 1. / (r5* 2. * (2*M_PI)*(2*M_PI) * i._step *
      allbeads1.size()*(allbeads2.size() - 1.) / 2.); else i._norm = 1. /
      (r5* 2. * (2*M_PI)*(2*M_PI) * i._step * allbeads1.size() *
      allbeads2.size());*/
    } else {
      cout << "Volume correction off" << endl;
      i._norm = 1. / (4. * M_PI * i._step);
      /*if ((*iter)->get("type1").value() == (*iter)->get("type2").value())
           i._norm = 1. / (4. * M_PI * i._step *
       allbeads1.size()*(allbeads2.size() - 1.) / 2.); else i._norm = 1. / (4. *
       M_PI * i._step * allbeads1.size() * allbeads2.size());
       }*/
    }
  }
}
// create an entry for interactions
RDFCalculator::interaction_t *RDFCalculator::AddInteraction(Property *p) {
  string name = p->get("name").value();
  string group;

  group = "none";

  interaction_t *i = new interaction_t;
  i->_index = _interactions.size();
  _interactions[name] = i;
  getGroup(group)->_interactions.push_back(i);

  i->_step = p->get("step").as<double>();
  i->_min = p->get("min").as<double>();
  i->_max = p->get("max").as<double>();
  i->_norm = 1.0;
  i->_p = p;

  // initialize the current and average histogram
  int n = (int)((i->_max - i->_min) / i->_step + 1.000000001);

  i->_average.Initialize(i->_min, i->_max + i->_step, n);

  return i;
}

// end of trajectory, post processing data
void RDFCalculator::EndEvaluate() {
  if (_nframes > 0) {
    if (!_do_blocks) {
      WriteDist();
    }
  }
  // clear interactions and groups
  _interactions.clear();
  _groups.clear();
  if (!_processed_some_frames)
    throw std::runtime_error(
        "no frames were processed. Please check your input");
}

// load options from xml file
void RDFCalculator::LoadOptions(const string &file) {
  load_property_from_xml(_options, file);
  _bonded = _options.Select("cg.bonded");
  _nonbonded = _options.Select("cg.non-bonded");
}

// evaluate current conformation
void RDFCalculator::Worker::EvalConfiguration(Topology *top,
                                              Topology *top_atom) {
  _cur_vol = 4.0 / 3.0 * M_PI * _rdfcalculator->_subvol_rad *
             _rdfcalculator->_subvol_rad * _rdfcalculator->_subvol_rad;
  // process non-bonded interactions
  DoNonbonded(top);
  // process bonded interactions
  DoBonded(top);
}

void RDFCalculator::ClearAverages() {
  map<string, interaction_t *>::iterator ic_iter;
  map<string, group_t *>::iterator group_iter;

  _nframes = 0;
  for (ic_iter = _interactions.begin(); ic_iter != _interactions.end();
       ++ic_iter)
    ic_iter->second->_average.Clear();

  for (group_iter = _groups.begin(); group_iter != _groups.end(); ++group_iter)
    group_iter->second->_corr.setZero();
}

class IMCNBSearchHandler {
 public:
  IMCNBSearchHandler(HistogramNew *hist, double subvol_rad, vec boxc,
                     bool do_vol_corr)
      : _hist(hist),
        _subvol_rad(subvol_rad),
        _boxc(boxc),
        _do_vol_corr(do_vol_corr) {}

  HistogramNew *_hist;
  double _subvol_rad;
  vec _boxc;  // center of box
  bool _do_vol_corr;

  bool FoundPair(Bead *b1, Bead *b2, const vec &r, const double dist) {

    if (_do_vol_corr) {
      double dr = abs(b1->Pos() - _boxc);
      if (dist + dr > _subvol_rad)
        // 2.0 is because everything is normalized to 4 PI
        _hist->Process(dist, 2.0 / SurfaceRatio(dist, dr));
      else
        _hist->Process(dist);

    } else {
      _hist->Process(dist);
    }
    return false;
  }

  double SurfaceRatio(double dist, double r) {
    // r: distance of particle from ex center
    // dist: distance between particles
    return (1.0 + (_subvol_rad * _subvol_rad - r * r - dist * dist) /
                      (2.0 * dist * r));
  }
};

// process non-bonded interactions for current frame
void RDFCalculator::Worker::DoNonbonded(Topology *top) {
  for (list<Property *>::iterator iter = _rdfcalculator->_nonbonded.begin();
       iter != _rdfcalculator->_nonbonded.end(); ++iter) {
    string name = (*iter)->get("name").value();

    interaction_t &i = *_rdfcalculator->_interactions[name];

    // generate the bead lists
    BeadList beads1, beads2;

    beads1.GenerateInSphericalSubvolume(*top, (*iter)->get("type1").value(),
                                        _rdfcalculator->_boxc,
                                        _rdfcalculator->_subvol_rad);
    beads2.GenerateInSphericalSubvolume(*top, (*iter)->get("type2").value(),
                                        _rdfcalculator->_boxc,
                                        _rdfcalculator->_subvol_rad);

    _cur_beadlist_1_count = beads1.size();
    _cur_beadlist_2_count = beads2.size();

    // same types, so put factor 1/2 because of already counted interactions
    if ((*iter)->get("type1").value() == (*iter)->get("type2").value()) {
      _cur_beadlist_2_count /= 2.0;
    }

    // generate the neighbour list
    NBList *nb;

    bool gridsearch = true;

    if (_rdfcalculator->_options.exists("cg.nbsearch")) {
      if (_rdfcalculator->_options.get("cg.nbsearch").as<string>() == "grid")
        gridsearch = true;
      else if (_rdfcalculator->_options.get("cg.nbsearch").as<string>() ==
               "simple")
        gridsearch = false;
      else
        throw std::runtime_error("cg.nbsearch invalid, can be grid or simple");
    }
    if (gridsearch)
      nb = new NBListGrid();
    else
      nb = new NBList();

    nb->setCutoff(i._max + i._step);

    // clear the current histogram
    _current_hists[i._index].Clear();

    IMCNBSearchHandler h(&(_current_hists[i._index]),
                         _rdfcalculator->_subvol_rad, _rdfcalculator->_boxc,
                         _rdfcalculator->_do_vol_corr);
    nb->SetMatchFunction(&h, &IMCNBSearchHandler::FoundPair);

    // is it same types or different types?
    if ((*iter)->get("type1").value() == (*iter)->get("type2").value())
      nb->Generate(beads1);
    else
      nb->Generate(beads1, beads2);

    // store particle number in subvolume for each interaction
    i._avg_beadlist_1_count.Process(_cur_beadlist_1_count);
    i._avg_beadlist_2_count.Process(_cur_beadlist_2_count);

    // process all pairs
    /*NBList::iterator pair_iter;
    for(pair_iter = nb->begin(); pair_iter!=nb->end();++pair_iter) {
            _current_hists[i._index].Process((*pair_iter)->dist());
    }*/

    delete nb;
  }
}

// process non-bonded interactions for current frame
void RDFCalculator::Worker::DoBonded(Topology *top) {
  for (list<Property *>::iterator iter = _rdfcalculator->_bonded.begin();
       iter != _rdfcalculator->_bonded.end(); ++iter) {
    string name = (*iter)->get("name").value();

    interaction_t &i = *_rdfcalculator->_interactions[name];

    // clear the current histogram
    _current_hists[i._index].Clear();

    // now fill with new data
    std::list<Interaction *> list = top->InteractionsInGroup(name);

    std::list<Interaction *>::iterator ic_iter;
    for (ic_iter = list.begin(); ic_iter != list.end(); ++ic_iter) {
      Interaction *ic = *ic_iter;
      double v = ic->EvaluateVar(*top);
      _current_hists[i._index].Process(v);
    }
  }
}

// returns a group, creates it if doesn't exist
RDFCalculator::group_t *RDFCalculator::getGroup(const string &name) {
  map<string, group_t *>::iterator iter;
  iter = _groups.find(name);
  if (iter == _groups.end()) {
    return _groups[name] = new group_t;
  }
  return (*iter).second;
}

// write the distribution function
void RDFCalculator::WriteDist(const string &suffix) {
  map<string, interaction_t *>::iterator iter;

  // for all interactions
  for (iter = _interactions.begin(); iter != _interactions.end(); ++iter) {
    // calculate the rdf
    Table &t = iter->second->_average.data();
    Table dist(t);

    /* OLD WRONG: correct for real density like below!
    double VolumeSq = _avg_vol.getAvg()*_avg_vol.getAvg();
    for (int i=0; i< dist.size(); i++){
        if (dist.x(i)>0) // avoid division through zero
            dist.y(i)=
    dist.y(i)*iter->second->_norm*VolumeSq/(AnalyticVolumeCorrection(dist.x(i)/_subvol_rad));
        else
            dist.y(i)=0.0;
    }*/

    // dist.y() = _avg_vol.getAvg()*iter->second->_norm *
    // factor 1/2 if beadlist1==beadlist2 already inclyded in
    // _avg_beadlist_2_count!!
    iter->second->_norm /= (iter->second->_avg_beadlist_1_count.getAvg() *
                            iter->second->_avg_beadlist_2_count.getAvg());
    dist.y() = _avg_vol.getAvg() * iter->second->_norm *
               dist.y().cwiseQuotient(dist.x().cwiseAbs2());

    dist.Save((iter->first) + suffix + ".dist.new");
    cout << "written " << (iter->first) + suffix + ".dist.new\n";

    cout << "Avg. number of particles in subvol for " << (iter->first) << endl;
    cout << "beadlist 1: " << iter->second->_avg_beadlist_1_count.getAvg()
         << endl;
    cout << "beadlist 2: " << iter->second->_avg_beadlist_2_count.getAvg()
         << endl;
  }

  cout << "Volume used for normalization: " << _avg_vol.getAvg() << endl;
}

CsgApplication::Worker *RDFCalculator::ForkWorker() {
  RDFCalculator::Worker *worker;
  worker = new RDFCalculator::Worker;
  map<string, interaction_t *>::iterator ic_iter;

  worker->_current_hists.resize(_interactions.size());
  worker->_rdfcalculator = this;

  for (ic_iter = _interactions.begin(); ic_iter != _interactions.end();
       ++ic_iter) {
    interaction_t *i = ic_iter->second;
    worker->_current_hists[i->_index].Initialize(
        i->_average.getMin(), i->_average.getMax(), i->_average.getNBins());
  }
  return worker;
}

void RDFCalculator::MergeWorker(CsgApplication::Worker *worker_) {
  _processed_some_frames = true;
  RDFCalculator::Worker *worker =
      dynamic_cast<RDFCalculator::Worker *>(worker_);
  // update the average
  map<string, interaction_t *>::iterator ic_iter;
  // map<string, group_t *>::iterator group_iter;

  ++_nframes;

  _avg_vol.Process(worker->_cur_vol);

  for (ic_iter = _interactions.begin(); ic_iter != _interactions.end();
       ++ic_iter) {
    interaction_t *i = ic_iter->second;
    i->_average.data().y() =
        (((double)_nframes - 1.0) * i->_average.data().y() +
         worker->_current_hists[i->_index].data().y()) /
        (double)_nframes;
  }

  if (_write_every != 0) {
    if ((_nframes % _write_every) == 0) {
      _nblock++;
      string suffix = string("_") + boost::lexical_cast<string>(_nblock);
      WriteDist(suffix);
      if (_do_blocks) ClearAverages();
    }
  }
}

}  // namespace csg
}  // namespace votca

#endif /* _RDFCALCULATOR_H */

#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>
#include <votca/csg/cgengine.h>
#include <votca/csg/version.h>

using namespace std;
using namespace votca::csg;
using namespace votca::tools;

#include <stdlib.h>
#include <votca/csg/csgapplication.h>

// using namespace votca::tools;
using namespace std;
using namespace votca::csg;

class CsgStatApp : public CsgApplication {
 public:
  CsgStatApp() : _write_every(0) {}

  string ProgramName() { return "csg_partial_rdf"; }
  void HelpText(ostream &out);

  bool DoTrajectory() { return true; }
  bool DoMapping() { return true; }
  bool DoMappingDefault(void) { return false; }
  bool DoThreaded() { return true; }
  bool SynchronizeThreads() { return true; }
  void Initialize();
  bool EvaluateOptions();

  void BeginEvaluate(Topology *top, Topology *top_ref);
  void EndEvaluate();

  CsgApplication::Worker *ForkWorker() { return _rdf_calculator.ForkWorker(); }

  void MergeWorker(CsgApplication::Worker *worker) {
    _rdf_calculator.MergeWorker(worker);
  }

 public:
  RDFCalculator _rdf_calculator;
  int _write_every;
};

void CsgStatApp::HelpText(ostream &out) {
  out << "Calculate spatially confined rdfs";
}

void CsgStatApp::Initialize() {
  CsgApplication::Initialize();
  AddProgramOptions("Specific options")("options",
                                        boost::program_options::value<string>(),
                                        "  options file defining the rdfs")(
      "subvolume_radius", boost::program_options::value<double>(),
      "Rdf calc. in spherical subvolume of this radius (from center of box)")(
      "do-vol-corr", "Correct for subvolume truncation in rdf")(
      "write-every", boost::program_options::value<int>(&_write_every),
      " (UNIMPLEMENTED) write after every block of this length, "
      "if --blocking   is set, the averages are cleared after every output")(
      "do-blocks", "  write output for blocking analysis");
}

bool CsgStatApp::EvaluateOptions() {
  CsgApplication::EvaluateOptions();
  CheckRequired("options");
  CheckRequired("subvolume_radius");
  CheckRequired("trj", "no trajectory file specified");

  _rdf_calculator.LoadOptions(OptionsMap()["options"].as<string>());

  _rdf_calculator.SetSubvolRadius(
      OptionsMap()["subvolume_radius"].as<double>());

  _rdf_calculator.WriteEvery(_write_every);
  if (OptionsMap().count("do-blocks")) _rdf_calculator.DoBlocks(true);

  if (OptionsMap().count("do-vol-corr"))
    _rdf_calculator.DoVolumeCorrection(true);

  _rdf_calculator.Initialize();
  return true;
}

void CsgStatApp::BeginEvaluate(Topology *top, Topology *top_ref) {
  _rdf_calculator.BeginEvaluate(top, top_ref);
}

void CsgStatApp::EndEvaluate() { _rdf_calculator.EndEvaluate(); }

int main(int argc, char **argv) {
  CsgStatApp app;
  app.Exec(argc, argv);
}
